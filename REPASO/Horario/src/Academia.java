import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Academia {

	private ArrayList<Profesor> horario = new ArrayList<>();
	private ArrayList<Profesor> profesores = new ArrayList<>();

	public ArrayList<Profesor> getHorario() {
		return horario;
	}

	public void setHorario(ArrayList<Profesor> horario) {
		this.horario = horario;
	}

	public ArrayList<Profesor> getProfesores() {
		return profesores;
	}

	public void setProfesores(ArrayList<Profesor> profesores) {
		this.profesores = profesores;
	}

	public void generarProfesores() {
		Scanner sc = new Scanner(System.in);

		for (int i = 0; i < 4; i++) {
			System.out.println("Introduce un profesor");
			String profesor = sc.next();
			Profesor p = new Profesor(profesor);
			profesores.add(p);
			System.out.println("Profesor " + profesor + " introducido con �xito");

		}
	}

	public void generarHorario() {

		for (int i = 0; i < 12; i++) {
			Random random = new Random();
			int numero = random.nextInt(profesores.size()); // Genera n�mero random seg�n cu�ntos profesores haya
			//bSystem.out.println(numero);
			// Comprobamos que un profesor no tiene m�s de 3 turnos
			if (profesores.get(numero).getContador() < 3) {
				// Comprobamos que un profesor no aparezca dos veces el mismo d�a.
				// Verificamos la impar que es la segunda del dia
				if (i % 2 != 0 && profesores.get(numero) == horario.get(i - 1)) {
					i--; // En caso de repetirse se vuelve a intentar
				} else {
					horario.add(profesores.get(numero));
					int contador = profesores.get(numero).getContador();
					profesores.get(numero).setContador(contador + 1);
				}
			} else {
				profesores.remove(numero);
				i--;
			}

		}
	}

	public void mostrarHorario() {
		System.out.println("************ HORARIO ACADEMIA *************");
		System.out.println("Lunes: " + horario.get(0).getNombre() + "--" + horario.get(1).getNombre());
		System.out.println("Martes: " + horario.get(2).getNombre() + "--" + horario.get(3).getNombre());
		System.out.println("Miercoles: " + horario.get(4).getNombre() + "--" + horario.get(5).getNombre());
		System.out.println("Jueves: " + horario.get(6).getNombre() + "--" + horario.get(7).getNombre());
		System.out.println("Viernes: " + horario.get(8).getNombre() + "--" + horario.get(9).getNombre());
		System.out.println("Sabado: " + horario.get(10).getNombre() + "--" + horario.get(11).getNombre());

	}
}
