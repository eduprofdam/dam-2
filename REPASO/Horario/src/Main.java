import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Academia academia = new Academia();

		academia.generarProfesores();
		academia.generarHorario();
		academia.mostrarHorario();

	}
}
