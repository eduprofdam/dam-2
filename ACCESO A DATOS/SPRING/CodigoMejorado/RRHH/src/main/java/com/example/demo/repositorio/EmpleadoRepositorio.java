package com.example.demo.repositorio;

import jakarta.persistence.Entity;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.modelo.Empleado;


public interface EmpleadoRepositorio extends JpaRepository<Empleado, Integer> {
}
