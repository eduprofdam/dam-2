package com.example.demo.servicio;

import org.springframework.http.ResponseEntity;


import com.example.demo.modelo.Empleado;
import com.example.demo.response.EmpleadoResponseRest;

public interface IEmpleadoService {
	
	public ResponseEntity<EmpleadoResponseRest> buscarEmpleados();
	public ResponseEntity<EmpleadoResponseRest> buscarPorId(int id);
	public ResponseEntity<EmpleadoResponseRest> crear(Empleado empleado);
	public ResponseEntity<EmpleadoResponseRest> actualizar(Empleado empleado, int id);
	public ResponseEntity<EmpleadoResponseRest> eliminar (int id);

}
