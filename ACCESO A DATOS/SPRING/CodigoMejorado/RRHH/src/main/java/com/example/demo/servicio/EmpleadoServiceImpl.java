package com.example.demo.servicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.modelo.Empleado;
import com.example.demo.modelo.dao.IEmpleadoDao;
import com.example.demo.response.EmpleadoResponseRest;



@Service
public class EmpleadoServiceImpl implements IEmpleadoService{

	private static final Logger log = LoggerFactory.getLogger(EmpleadoServiceImpl.class);
	
	@Autowired
	private IEmpleadoDao empleadoDao;
	
	@Override
	@Transactional(readOnly = true)
	public ResponseEntity<EmpleadoResponseRest> buscarEmpleados() {
		
		log.info("inicio metodo buscarEmpleados()");
		
		EmpleadoResponseRest response = new EmpleadoResponseRest();
		
		try {
			
			List<Empleado> empleado = (List<Empleado>) empleadoDao.findAll();
			
			response.getEmpleadoResponse().setCategoria(empleado);
			
			response.setMetada("Respuesta ok", "00", "Respuesta exitosa");
			
		} catch (Exception e) {
			response.setMetada("Respuesta nok", "-1", "Error al consulta empleados");
			log.error("error al consultar empleados: ", e.getMessage());
			e.getStackTrace();
			return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.OK); //devuelve 200
	}

	@Override
	@Transactional(readOnly = true)
	public ResponseEntity<EmpleadoResponseRest> buscarPorId(int id) {
		
		log.info("Inicio metodo buscarPorId)");
		
		EmpleadoResponseRest response = new EmpleadoResponseRest();
		List<Empleado> list = new ArrayList<>();
		
		try {
			
			Optional<Empleado> empleado = empleadoDao.findById(id);
			
			if (empleado.isPresent()) {
				list.add(empleado.get());
				response.getEmpleadoResponse().setCategoria(list);
			} else {
				log.error("Error en consultar empleado");
				response.setMetada("Respuesta nok", "-1", "Empleado no encontrado");
				return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.NOT_FOUND);
			}
			
		} catch (Exception e) {
			log.error("Error en consultar empleado");
			response.setMetada("Respuesta nok", "-1", "Error al consultar empleado");
			return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.setMetada("Respuesta ok", "00", "Respuesta exitosa");
		return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.OK); //devuelve 200		
	}

	@Override
	@Transactional
	public ResponseEntity<EmpleadoResponseRest> crear(Empleado empleado) {
		log.info("Inicio metodo crear empleado");
		
		EmpleadoResponseRest response = new EmpleadoResponseRest();
		List<Empleado> list = new ArrayList<>();
		
		try {
			
			Empleado empleadoGuardado = empleadoDao.save(empleado);
			
			if( empleadoGuardado != null) {
				list.add(empleadoGuardado);
				response.getEmpleadoResponse().setCategoria(list);
			} else {
				log.error("Error en grabar empleado");
				response.setMetada("Respuesta nok", "-1", "Empleado no guardada");
				return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.BAD_REQUEST);
			}
			
		} catch( Exception e) {
			log.error("Error en grabar empleado");
			response.setMetada("Respuesta nok", "-1", "Error al grabar empleado");
			return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.setMetada("Respuesta ok", "00", "Empleado creado");
		return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.OK); //devuelve 200
	}

	@Override
	@Transactional
	public ResponseEntity<EmpleadoResponseRest> actualizar(Empleado empleado, int id) {
		
		log.info("Inicio metodo actualizar");
		
		EmpleadoResponseRest response = new EmpleadoResponseRest();
		List<Empleado> list = new ArrayList<>();
		
		try {
			
			Optional<Empleado> empleadoBuscado = empleadoDao.findById(id);
			
			if (empleadoBuscado.isPresent()) {
				empleadoBuscado.get().setNombre(empleado.getNombre());
				 empleadoBuscado.get().setDepartamento(empleado.getDepartamento());
				 empleadoBuscado.get().setSueldo(empleado.getSueldo());
				 
				 Empleado empleadoActualizar = empleadoDao.save(empleadoBuscado.get()); //actualizando
				 
				 if( empleadoActualizar != null ) {
					 response.setMetada("Respuesta ok", "00", "Empleado actualizado");
					 list.add(empleadoActualizar);
					 response.getEmpleadoResponse().setCategoria(list);
				 } else {
					 log.error("error en actualizar categoria");
					 response.setMetada("Respuesta nok", "-1", "Empleado no actualizado");
					 return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.BAD_REQUEST);
				 }
				 
				 
			} else {
				log.error("error en actualizar empleado");
				response.setMetada("Respuesta nok", "-1", "Empleado no actualizado");
				return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.NOT_FOUND);
			}
			
		} catch ( Exception e) {
			log.error("error en actualizar empleado", e.getMessage());
			e.getStackTrace();
			response.setMetada("Respuesta nok", "-1", "Empleado no actualizado");
			return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.OK);
		
	}

	@Override
	@Transactional
	public ResponseEntity<EmpleadoResponseRest> eliminar(int id) {
		
		log.info("Inicio metodo eliminar categoria");
		
		EmpleadoResponseRest response = new EmpleadoResponseRest();
		
		 try {
			 
			 //eliminamos el registro
			 empleadoDao.deleteById(id);
			 response.setMetada("Respuesta ok", "00", "empleado eliminado");
			 
		 } catch (Exception e) {
			log.error("error en eliminar empleado", e.getMessage());
			e.getStackTrace();
			response.setMetada("Respuesta nok", "-1", "Categoria no eliminada");
			return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR); 
		 }
		 
		 return new ResponseEntity<EmpleadoResponseRest>(response, HttpStatus.OK);
		
	}

}
