package com.eduardo.demo.controlador;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.eduardo.demo.excepcion.RecursoNoEncontradoExcepcion;
import com.eduardo.demo.modelo.Empleado;
import com.eduardo.demo.servicio.IEmpleadoServicio;

//http://localhost:8080
//localhost:8080/rh-app
@RestController
@RequestMapping("rh-app")
//@CrossOrigin(value="http://localhost:8081")
public class EmpleadoControlador {
	
	private static final Logger logger = LoggerFactory.getLogger(EmpleadoControlador.class);
	

	@Autowired
	private IEmpleadoServicio empleadoServicio;
	
	//localhost:8080/rh-app/empleados
	@GetMapping("/empleados")
	public List<Empleado> obtenerEmpleados(){
		
		List<Empleado> empleados = empleadoServicio.listarEmpleados();
		empleados.forEach(empleado -> logger.info(empleado.toString()));
		
		return empleados;
	}
	
	@PostMapping("/empleados")
	public Empleado agregarEmpleado(@RequestBody Empleado empleado) {
		
		logger.info("Empleado a agregar"+ empleado);
		return empleadoServicio.guardarEmpleado(empleado);
	}
	
	@GetMapping("/empleados/{id}")
	public ResponseEntity<Empleado> obtenerEmpleadoPorId( @PathVariable int id){
		Empleado empleado = empleadoServicio.buscarEmpleadoPorId(id);
		
		if (empleado == null) {
			throw new RecursoNoEncontradoExcepcion("no se encontró el id");
			
		}else {
			return ResponseEntity.ok(empleado);
		}
		
	}
	@DeleteMapping("/empleados/{id}")
	public ResponseEntity <Map<String,Boolean>>eliminarEmpleado(@PathVariable int id){
		Empleado empleado = empleadoServicio.buscarEmpleadoPorId(id);
		
		if (empleado == null) {
			throw new RecursoNoEncontradoExcepcion("no se encontró el id");
			
		}else {
			empleadoServicio.eliminarEmpleado(empleado);
			
			// JSON ( eliminado: true)
			Map<String,Boolean> respuesta = new HashMap<>();
			respuesta.put("eliminado",Boolean.TRUE);
			return ResponseEntity.ok(respuesta);
		}
		
	}
	

	
	@PutMapping("/empleados/{id}")
	public ResponseEntity<Empleado> actualizarEmpleado (@PathVariable int id,
			@RequestBody Empleado empleadoRecibido){
		Empleado empleado = empleadoServicio.buscarEmpleadoPorId(id);
		if (empleado == null) {
			throw new RecursoNoEncontradoExcepcion("no se encontró el id");
			
		}else {
			empleado.setNombre(empleadoRecibido.getNombre());
			empleado.setDepartamento(empleado.getDepartamento());
			empleado.setSueldo(empleado.getSueldo());
			empleadoServicio.guardarEmpleado(empleado);
			
			return ResponseEntity.ok(empleado);
		}
		
	}
}
