package com.eduardo.demo.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eduardo.demo.modelo.Empleado;

public interface EmpleadoRepositorio extends JpaRepository<Empleado, Integer>{
	
	

}
