<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<truchas>
<xsl:for-each select="data/row">
<xsl:sort select="id" data-type="number"/>
<xsl:if test="estado= 'VIVA'">
<xsl:if test="estado_reproductivo!= 0">
<trucha>
<xsl:attribute name="id"><xsl:value-of select="id"/></xsl:attribute>
<xsl:attribute name="qr"><xsl:value-of select="qr"/></xsl:attribute>
<tanque><xsl:value-of select="tanque"/></tanque>
<genotipada><xsl:choose>
<xsl:when test="genotipada=1">SI</xsl:when>
<xsl:otherwise>NO</xsl:otherwise>
</xsl:choose>
</genotipada>
<estado_reproductivo>
<xsl:choose>
  <xsl:when test="estado_reproductivo=1">BLANCA</xsl:when>
  <xsl:when test="estado_reproductivo=2">VERDE</xsl:when>
  <xsl:otherwise>MADURA</xsl:otherwise>
</xsl:choose>
</estado_reproductivo>
<mediciones>
  <tamaño><xsl:value-of select="mida"/></tamaño>
  <peso><xsl:value-of select="peso"/></peso>
</mediciones>
</trucha>
</xsl:if>
</xsl:if>
</xsl:for-each>
</truchas>
</xsl:template>
</xsl:stylesheet>