package Prepared;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MainPrepared {

	public static void main(String[] args) {
		
		
		Conexion con = new Conexion("jdbc:mysql://localhost/dam", "root", "admin");
		
		con.establecerConexion();
		Connection connection = con.getConnection();
		
		PreparedStatement stmt ;
		
		try {
			stmt = connection.prepareStatement("select * from alumnos where nombre like ? and edad= ? limit ?");
			stmt.setString(1, "pepe");
			stmt.setInt(2, 23);
			stmt.setInt(3,1);
			
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				System.out.println(rs.getInt(1)+ "---");
				System.out.println(rs.getString(2));
				
			}
			
		} catch (SQLException e) {
			System.out.println("Error al realizar la consulta en la BBDD");
			e.printStackTrace();
		}
		
		con.cerrarConexion();
		
	}

}
