package Conexion;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {
	public static void main(String[] args) throws SQLException {

		Conexion con = new Conexion("jdbc:mysql://localhost/dam", "root", "admin");

		con.establecerConexion();
		
		Statement st = con.getConnection().createStatement();

		String query = "select * from alumnos";

		ResultSet rs = st.executeQuery(query);

		while (rs.next()) {
			System.out.println("Id " + rs.getInt(1) + " Nombre:" + rs.getString(2) + ". Edad: " + rs.getInt(3));
		}
		
	con.cerrarConexion();

	}

}
