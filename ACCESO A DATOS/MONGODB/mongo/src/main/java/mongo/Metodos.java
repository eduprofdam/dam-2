package mongo;

import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class Metodos {
	
	public MongoCollection <Document>listarCollection (MongoDatabase db,String nombre) {
		MongoCollection<Document> collection = db.getCollection(nombre); // listingReviews
		
		Document doc = collection.find().first();
		
		System.out.println(doc.toJson());
		
		return collection;
		
	}
	
	public void buscar(MongoDatabase db, String nombreColeccion, String key, String value) {
		
		MongoCollection<Document> collection = db.getCollection(nombreColeccion);
		
		Document findDocument = new Document(key, value);
		
		MongoCursor <Document> resultDocument = collection.find(findDocument).iterator();
		
		System.out.println("Listado de todos los resultado:");
		
		while(resultDocument.hasNext()) {
			System.out.println("nombre: "+ resultDocument.next().getString(key));
		}
		
	}
	

}
