package xstream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class Main {

	public static void main(String[] args) {
		
		
		
		Libro l1 = new  Libro ("libro 1","novela", "IHGHF5","Anaya");
		
		XStream xstream = new XStream(new DomDriver());
		
		xstream.alias("libro", Libro.class);
		String salida = xstream.toXML(l1);
		System.out.println(salida);
		
		File f1 = new File("archivoXML");
		try {
			f1.createNewFile();
		} catch (IOException e) {
			System.out.println("No se ha podido crear el documento");
			e.printStackTrace();
		}
		
		try {
			FileWriter fw = new FileWriter (f1);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(salida);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
