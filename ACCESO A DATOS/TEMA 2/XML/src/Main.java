import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Main {

	public static void main(String[] args) {


		// OPTIMIZAR CODIGO Y MODULAR
		 //crearBuilder()
		// listarElementos (NodeList)
		// leerXml(archivo)
	
		
		//leerXML("test.xml","Tests");
		escribirXML();

	}
	
	public static DocumentBuilder createBuilder() {
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return builder;
		
		
	}
	
	public static void listarElementos(NodeList list, String elem1, String elem2) {
		
		for(int i = 0; i<list.getLength();i++) {
			Node node = list.item(i);
			
			if(node.getNodeType()== Node.ELEMENT_NODE) {
				Element elemento = (Element) node;
				
				// IMPRIME ATRIBUTOS
				String id = elemento.getAttributes().getNamedItem("TestId").getNodeValue();
				System.out.println(node.getNodeName()+ "\t"+ id);
				System.out.println();
				
				// IMPRIME ELEMENTOS
				
				System.out.println(elem1+ getNodo(elem1,elemento));
				System.out.println(elem2+ getNodo(elem2,elemento));
			}
			
		}
	
	} 
	public static String getNodo (String etiqueta, Element elem) {
		NodeList nodo = elem.getElementsByTagName(etiqueta).item(0).getChildNodes();
		Node valorNodo = (Node) nodo.item(0);
		
		return valorNodo.getNodeValue(); // devuelve el valor del nodo
	}
	
	public static void leerXML(String archivo, String raiz) {
		
		Path path = Path.of(archivo);
		File xml = path.toFile(); // Convertir un path en file;
		
		DocumentBuilder builder = createBuilder();
		Document document = null;
		try {
			document = builder.parse(xml);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		NodeList listaInicial = document.getElementsByTagName(raiz).item(0).getChildNodes();
		listarElementos(listaInicial,"Name","CommandLine");
		
	}
	
	public static void escribirXML() {
		DocumentBuilder builder = createBuilder();
		DOMImplementation implementation = builder.getDOMImplementation();
		Document document = implementation.createDocument(null, null, null);
		document.setXmlVersion("1.0");
		document.setXmlStandalone(true);
		
		Element alumnos = document.createElement("Alumnos");
		document.appendChild(alumnos);
		
		//Alumno
		Element alumno = document.createElement("Alumno");
		alumno.setAttribute("nombre", "Eduardo");
		alumno.setAttribute("edad", "23");
		alumnos.appendChild(alumno);
		
		//Direccion
		Element direccion = document.createElement("direccion");
		direccion.setTextContent("Gran via 1"); // <direccion> Gran via </direccion>
		alumno.appendChild(direccion);
		Element telefono = document.createElement("telefono");
		telefono.setTextContent("929292929");
		alumno.appendChild(telefono);
		
		
		Source origen = new DOMSource(document);
		//File archivoResultado = new File ("alumnos.xml");
		Result result = new StreamResult(new File ("alumnos.xml"));
		
		Transformer transformer = null;
		
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
			
			// ESTAS DOS LINEAS SIGUIENTES NOS PERMITEN ESCRIBIR EN FORMATO LEGIBLE, CON INDENTACION
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			System.out.println("Error al crear el transformer");
			e.printStackTrace();
		}
		
		try {
			transformer.transform(origen, result);
		} catch (TransformerException e) {
			System.out.println("Error al transformar el archivo");
			e.printStackTrace();
		}
		
		
		
		
		/*<alumnos>
		<alumno nombre="Eduardo" edad="23">
		<direccion> Calle gran via</direccion>
		<telefono> 56675654</telefono>
		
		</alumno>
		
		</alumnos>
		*/
	}

	
	

}
