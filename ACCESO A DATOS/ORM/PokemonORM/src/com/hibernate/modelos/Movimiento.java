package com.hibernate.modelos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "Movimiento", uniqueConstraints = @UniqueConstraint(columnNames = { "movimiento_id" }))
public class Movimiento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "movimiento_id", unique = true, nullable = false)
	private int movimiento_id;

	@Column(name = "nombre", length = 45)
	private String nombre;

	@Column(name = "tipo", length = 45)
	private String tipo;

	@Column(name = "poder", length = 45)
	private String poder;

	@Column(name = "precision_movimiento")
	private int precision_movimiento;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "Pokemon_Movimientos", joinColumns = {
			@JoinColumn(name = "movimiento_id") }, inverseJoinColumns = { @JoinColumn(name = "pokemon_id") })
	private List<Pokemon> pokemons = new ArrayList<>();

	public Movimiento() {
		super();
	}

	public int getMovimiento_id() {
		return movimiento_id;
	}

	public void setMovimiento_id(int movimiento_id) {
		this.movimiento_id = movimiento_id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getPoder() {
		return poder;
	}

	public void setPoder(String poder) {
		this.poder = poder;
	}

	public int getPrecision_movimiento() {
		return precision_movimiento;
	}

	public void setPrecision_movimiento(int precision_movimiento) {
		this.precision_movimiento = precision_movimiento;
	}

	public List<Pokemon> getPokemons() {
		return pokemons;
	}

	public void setPokemons(List<Pokemon> pokemons) {
		this.pokemons = pokemons;
	}

}
