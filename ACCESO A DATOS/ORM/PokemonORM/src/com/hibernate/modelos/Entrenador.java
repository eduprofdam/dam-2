package com.hibernate.modelos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "Entrenador", uniqueConstraints = @UniqueConstraint(columnNames = { "id" }))
public class Entrenador {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	
	 @Column(name="nombre", length=45)
	private String nombre;
	
	 @Column(name="edad")
	private int edad;
	 
	 @Column(name="ciudad", length=15)
	private String ciudad;

	 @OneToMany(mappedBy="entrenador")
	private List<Pokemon> pokemons = new ArrayList<>();

	public Entrenador() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public List<Pokemon> getPokemons() {
		return pokemons;
	}

	public void setPokemons(List<Pokemon> pokemons) {
		this.pokemons = pokemons;
	}
	
	

}
