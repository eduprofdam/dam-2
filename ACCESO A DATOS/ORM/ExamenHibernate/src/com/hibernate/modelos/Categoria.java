package com.hibernate.modelos;


import java.util.ArrayList;

import java.util.List;
import javax.persistence.*;


@Entity
@Table(name="Categoria")
public class Categoria {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_categoria",unique=true, nullable= false)
     private int idCategoria;
	 
	@Column(length=45)
     private String nombre;
	 
	@Column(length=45)
     private String descripcion;
	 
	 @OneToMany(mappedBy="categoria")
     private List<Producto> productos = new ArrayList<Producto>();

    public Categoria() {
    }

   
   
   
    public int getIdCategoria() {
        return this.idCategoria;
    }
    
    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    
    
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	@Override
	public String toString() {
		return "Categoria [idCategoria=" + idCategoria + ", nombre=" + nombre + ", descripcion=" + descripcion
				+ ", productos=" + productos + "]";
	}


 



}