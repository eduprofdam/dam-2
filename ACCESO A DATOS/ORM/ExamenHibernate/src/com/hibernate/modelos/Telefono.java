package com.hibernate.modelos;


import javax.persistence.*;




@Entity
@Table(name="Telefono",uniqueConstraints = @UniqueConstraint(columnNames = {"id_telefono"}))
public class Telefono {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_telefono")
     private int idTelefono;
	 
	
     private String numero;
	 
	 
	 @ManyToOne(cascade = CascadeType.ALL)
	 @JoinColumn(name="id_proveedor")
     private Proveedor proveedor;

    public Telefono() {
    }

	public int getIdTelefono() {
		return idTelefono;
	}

	public void setIdTelefono(int idTelefono) {
		this.idTelefono = idTelefono;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	
	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		proveedor = proveedor;
	}

	@Override
	public String toString() {
		return "Telefono [idTelefono=" + idTelefono + ", numero=" + numero + ", proveedor=" + proveedor + "]";
	}

	




	
   




}


