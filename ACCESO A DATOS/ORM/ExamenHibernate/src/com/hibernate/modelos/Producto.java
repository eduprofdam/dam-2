package com.hibernate.modelos;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="producto",uniqueConstraints = @UniqueConstraint(columnNames = {"id_producto"}))
public class Producto {
	
	
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="id_producto")
	private int idProducto;


@ManyToOne()
@JoinColumn(name="id_proveedor")
	private Proveedor proveedor;
	
	@ManyToOne()
	@JoinColumn(name="id_categoria")
	private Categoria categoria;

	
	private String nombre;


	private Double precio;

	
	private int stock;

	

	public Producto() {
	}

	
	@Override
	public String toString() {
		return "Producto [idProducto=" + idProducto + ", proveedor=" + proveedor + ", categoria=" + categoria
				+ ", nombre=" + nombre + ", precio=" + precio + ", stock=" + stock + "]";
	}


	public Categoria getCategoria() {
		return categoria;
	}


	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}


	public int getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public Proveedor getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getPrecio() {
		return this.precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public int getStock() {
		return this.stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	

}
