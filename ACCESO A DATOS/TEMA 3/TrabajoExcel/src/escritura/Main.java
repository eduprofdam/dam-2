package escritura;
import java.io.File;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		TrabajoExcel.escribirFichero();
		
		ArrayList<Usuario> lista = new ArrayList<>();
		lista.add(new Usuario("Eduardo","Sanz","654323"));
		lista.add(new Usuario("Mar�a","Garcia","334566778"));
		lista.add(new Usuario("Pepa","Gonzalez","65321334"));
		TrabajoExcel.escribirListaCompleta(lista);

	}

}
