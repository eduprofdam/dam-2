package escritura;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TrabajoExcel {
	public static void escribirFichero(){

	    XSSFWorkbook ficheroExcel = null;
	    ficheroExcel = new XSSFWorkbook();
	    XSSFSheet hoja = ficheroExcel.createSheet();
	    XSSFRow fila = hoja.createRow(0);
	    XSSFCell celda = fila.createCell(0);
	    Cell celdaTipo = fila.createCell(1, CellType.NUMERIC);
	    celda.setCellValue("Ejemplo");
	    celdaTipo.setCellValue(1);
	    try {
	        FileOutputStream fileOutput = new FileOutputStream("ejemplo2.xlsx");
	        ficheroExcel.write(fileOutput);
	        fileOutput.close();
	        ficheroExcel.close();
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	public static void escribirListaCompleta(ArrayList<Usuario> lista){

        XSSFWorkbook ficheroExcel = new XSSFWorkbook();
        XSSFSheet hoja = ficheroExcel.createSheet("usuarios");
        XSSFRow fila = hoja.createRow(0);
        XSSFCell celda;
        int numFilas=0;
       
        String[]cabeceras = new String[]{"Nombre","Apellido","T�lefono"};

        for (int i=0;i<cabeceras.length;i++){
            celda = fila.createCell(i);
            celda.setCellValue(cabeceras[i]);
        }
        numFilas++;

        for (Usuario u:lista) {
          
            fila = hoja.createRow(numFilas);
            celda = fila.createCell(0);
            celda.setCellValue(u.getNombre());
            celda = fila.createCell(1);
            celda.setCellValue(u.getApellido());
            celda = fila.createCell(2);
            celda.setCellValue(u.getTelefono());
            numFilas++;
        }

        try {
            FileOutputStream fileOutput = new FileOutputStream("listado.xlsx");
            ficheroExcel.write(fileOutput);
            fileOutput.close();
            ficheroExcel.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


