package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
	
	private static final String HOST= "localhost";
	private  static final String DB = "sakila";
	private static final String USER= "root";
	private static  final String PASSWORD = "admin";
	
	public  static Connection getMySQLConnection() {
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			String connectionURL = "jdbc:mysql://"+ HOST + ":3306/"+ DB;
			Connection conn = DriverManager.getConnection(connectionURL,USER,PASSWORD);
			return conn;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}

}
