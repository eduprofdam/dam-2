import java.sql.Connection;
import java.util.HashMap;

import model.Conexion;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class ReportGenerator {
	
	public static final String REPORTE_SIMPLE_PELICULAS= "reporte_peliculas.jasper";
	
	
	
	public static JasperPrint generarReporteSimplePeliculas() {

		Connection con = Conexion.getMySQLConnection();
		
		try {
			JasperPrint reporteLleno = JasperFillManager.fillReport(REPORTE_SIMPLE_PELICULAS,new HashMap(),con);
			return reporteLleno;
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
		
	}

}
