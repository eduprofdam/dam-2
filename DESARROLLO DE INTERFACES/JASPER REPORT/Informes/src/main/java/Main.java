import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class Main {

	public static void main(String[] args) {
		
		JasperPrint reporteLleno = ReportGenerator.generarReporteSimplePeliculas();
		
		try {
			JasperExportManager.exportReportToPdfFile(reporteLleno,"mis_peliculas.pdf");
			JasperViewer viewer = new JasperViewer(reporteLleno);
			viewer.setVisible(true);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}

}
