package com.mycompany.tablefx;



import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;

public class PrimaryController implements Initializable{

    @FXML
    private TableView<Coche> tb1Coches;

    @FXML
    private TableColumn colMarca;

    @FXML
    private TableColumn colModelo;

    @FXML
    private TableColumn colCv;

    @FXML
    private TextField txtMarca;

    @FXML
    private TextField txtModelo;

    @FXML
    private TextField txtCv;

    @FXML
    private Button btAgregar;

@FXML
private TextField txtFiltrarMarca;
    
    private ObservableList<Coche> coches;

    private ObservableList <Coche> filtroCoches;

    @FXML
    void agregarCoche(ActionEvent event) {
        try{
        String marca = this.txtMarca.getText();
        String modelo = this.txtModelo.getText();
        int cv = Integer.parseInt(this.txtCv.getText());
        Coche c = new Coche (marca, modelo,cv);
        coches.add(c);
        tb1Coches.setItems(coches);
        limpiarCeldas(txtMarca,txtModelo,txtCv);

    }catch(NumberFormatException e){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText("Caballos debe ser un número");
        alert.showAndWait();
    }
    }
    
    public void limpiarCeldas (TextField tfMarca, TextField tfModelo, TextField tfCV){
        
        tfMarca.setText(" ");
        tfModelo.setText(" ");
        tfCV.setText(" ");
    }

 @FXML
 public void filtrarModelo(KeyEvent event) {
String filtroMarca = this.txtFiltrarMarca.getText().toLowerCase();

if (filtroMarca.isEmpty()){
this.tb1Coches.setItems(coches);

}else{

this.filtroCoches.clear();

for (Coche c : this.coches){

if ( c.getMarca().toLowerCase().contains(filtroMarca)){
this.filtroCoches.add(c);

}

}
this.tb1Coches.setItems(filtroCoches);
    }


}
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        coches = FXCollections.observableArrayList();
        filtroCoches = FXCollections.observableArrayList();
        
        this.colMarca.setCellValueFactory(new  PropertyValueFactory("marca")); // Nombre del atributo de la clase Modelo, en este caso Coche
        this.colModelo.setCellValueFactory(new  PropertyValueFactory("modelo"));
        this.colCv.setCellValueFactory(new  PropertyValueFactory("cv"));
        
    }

}
