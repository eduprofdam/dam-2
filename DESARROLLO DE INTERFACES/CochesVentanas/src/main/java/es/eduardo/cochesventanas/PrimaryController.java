package es.eduardo.cochesventanas;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class PrimaryController implements Initializable {

    @FXML
    private Button btnAgregar;
    private TextField txtMarca;
    private TextField txtModelo;
    private TextField txtCv;
    @FXML
    private TableView<Coche> tblCoches;
    @FXML
    private TableColumn colMarca;
    @FXML
    private TableColumn colModelo;
    @FXML
    private TableColumn colCv;

    private ObservableList<Coche> coches;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        coches = FXCollections.observableArrayList();
        this.tblCoches.setItems(coches);

        this.colMarca.setCellValueFactory(new PropertyValueFactory("marca"));
        this.colModelo.setCellValueFactory(new PropertyValueFactory("modelo"));
        this.colCv.setCellValueFactory(new PropertyValueFactory("cv"));
    }

    @FXML
    private void agregarCoche(ActionEvent event) throws IOException {

        FXMLLoader loader= new FXMLLoader (getClass().getResource("CocheDialogVista.fxml"));
        
        Parent  root = loader.load();
        
        // Cojo el controlador
        
        CocheDialogControlador controlador = loader.getController();
        controlador.iniciarAtributos(coches);
        
        
        Scene scene = new Scene (root);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.showAndWait();
        
        Coche c = controlador.getCoche();
        if (c!= null){
            this.coches.add(c);
            this.tblCoches.refresh();
        }else{
             Alert  alert = new Alert (Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Error");
            alert.showAndWait();
        }
     
        
        
    }

}
