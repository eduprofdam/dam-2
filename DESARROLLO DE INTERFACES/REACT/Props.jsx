import styled from 'styled-components';
import PropTypes from  'prop-types';
export const StyledButton = styled.button`
    padding: 10px 20px;
    color: black;
    border: 2px solid black;
    background-color: yellow;
    margin: 20px;
`;

export const StyledDiv = styled.div`
    width: 550px;
    height:200px;
    border: 2px solid black;
    background-color:yellow;
`;

export function Button(props) {
    return (
        <>
            <StyledButton >{props.title}</StyledButton>
            <p>{props.subTitle}</p>
            <StyledDiv />
        </>
    );
}


Button.propTypes = {
    title:PropTypes.string.isRequired,
    subTitle:PropTypes.string,
    texto: PropTypes.string,

}

Button.defaultProps = {
    title:'no hay titulo',
    subTitle: 'no hay titulo',
    texto: 'no hay titulo',
}