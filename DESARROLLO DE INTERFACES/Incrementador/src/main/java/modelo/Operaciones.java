/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;


public class Operaciones {
    
    private int a;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public Operaciones(int a) {
        this.a = a;
    }
    
    public int incrementar(){
        
        return ++this.a;
    }
    
public int decrementar(){
        return --this.a;
    }
}

