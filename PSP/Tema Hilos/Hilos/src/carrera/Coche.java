package carrera;

import java.util.Random;

public class Coche extends Thread{
	
	private  String nombre;
	
	private Random random =new Random();
	
	

	public Coche(String nombre) {
		
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override                                                        
	public void run() {
		
		for (int i = 0; i < 50; i++) {
			try {
				sleep(random.nextInt(1)*1000L);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			
		}
		System.out.println("Ha llegado a la meta el coche "+ this.getNombre());
	}
	

}
