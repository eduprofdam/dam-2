package tablaMultiplicar;

public class Tabla extends Thread{
	
	private int num;

	public Tabla(int num) {
		super();
		this.num = num;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	@Override
	public void run() {
		
		System.out.println("Tabla del "+ this.getNum());
		
		for(int i= 0; i<10;i++) {
			System.out.println(num + "X "+ i * num);
		}
	}
	
	

}
