package tablaMultiplicar;

public class Main {

	public static void main(String[] args) throws InterruptedException {

		Tabla t1 = new Tabla(1);
		Tabla t2 = new Tabla(2);
		Tabla t3 = new Tabla(3);
		Tabla t4 = new Tabla(4);

		t1.start();

		t1.join();

		t2.start();
		t2.join();
		t3.start();
		t3.join();
		t4.start();
		t4.join();
	}

}
