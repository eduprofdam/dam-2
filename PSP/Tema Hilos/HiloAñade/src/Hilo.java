


public class Hilo extends Thread{
	
	Recurso recurso;
	
	public Hilo (Recurso recurso) {
		this.recurso = recurso;
	}

	@Override
	public void run() {
		
		int dato;
		
		dato = recurso.addNumero();
		System.out.println(getName()+ " - " + dato );
		
		
	}
	
	

}
