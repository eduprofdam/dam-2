
import java.util.concurrent.Semaphore;

class MiHilo implements Runnable {
    private Semaphore semaphore;
    private String nombre;

    public MiHilo(Semaphore semaphore, String nombre) {
        this.semaphore = semaphore;
        this.nombre = nombre;
    }

    public void run() {
        try {
            System.out.println(nombre + " esperando adquirir el sem�foro");
            semaphore.acquire();
            System.out.println(nombre + " ha adquirido el sem�foro");
            Thread.sleep(2000);
            System.out.println(nombre + " liberando el sem�foro");
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}









