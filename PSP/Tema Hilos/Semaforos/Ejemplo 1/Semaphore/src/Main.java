import java.util.concurrent.Semaphore;

public class Main {
	public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(1); // Crear un sem�foro con 1 permiso

        // Crear hilos
        Thread hilo1 = new Thread(new MiHilo(semaphore, "Hilo 1"));
        Thread hilo2 = new Thread(new MiHilo(semaphore, "Hilo 2"));
        Thread hilo3 = new Thread(new MiHilo(semaphore, "Hilo 3"));

        // Iniciar hilos
        hilo1.start();
        hilo2.start();
        hilo3.start();
    }
}
