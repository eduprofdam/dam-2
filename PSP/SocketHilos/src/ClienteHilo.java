import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class ClienteHilo extends Thread{
	
	private DataInputStream in;
	private DataOutputStream out;
	Scanner  sn = new Scanner(System.in);
	
	

	public DataInputStream getIn() {
		return in;
	}



	public void setIn(DataInputStream in) {
		this.in = in;
	}



	public DataOutputStream getOut() {
		return out;
	}



	public void setOut(DataOutputStream out) {
		this.out = out;
	}
	
	



	public ClienteHilo(DataInputStream in, DataOutputStream out) {
		super();
		this.in = in;
		this.out = out;
	}



	@Override
	public void run() {
		
		try {
			String mensaje = in.readUTF();
			System.out.println(mensaje);
			String nombre = sn.next();
			out.writeUTF(nombre);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	
}
