import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Servidor {

	public static void main(String[] args) throws IOException {
		
		
		ArrayList<String>nombres = new ArrayList<>();
		ServerSocket server = new ServerSocket(5000);
		Socket sc;
		
		System.out.println("Servidor iniciado");
		
		while(true) {
			sc = server.accept();
			
			DataOutputStream out = new DataOutputStream(sc.getOutputStream());
			DataInputStream in = new DataInputStream(sc.getInputStream());
			
			out.writeUTF("Indique tu nombre");
			String nombre = in.readUTF();
			nombres.add(nombre);
			
			leerArray(nombres);
		}

	}
	
	public  static void leerArray (ArrayList<String> nombres) {
		
		System.out.println("Los nombres de los clientes que se han conectado al servidor son:");
		
		for(int i= 0; i<nombres.size();i++) {
			System.out.println(nombres.get(i).toString());
		}
	}

}
