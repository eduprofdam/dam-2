
import java.io.Serializable;

public class Empleado implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private double salario;
    private String cargo;

    public Empleado(int id, double salario) {
        this.id = id;
        this.salario = salario;
        //this.cargo = salario > 1000 ? "Jefe" : "Empleado";
       
    }
    
    

    public Empleado(int id, double salario, String cargo) {
		super();
		this.id = id;
		this.salario = salario;
		this.cargo = cargo;
	}



	// Getters y Setters
    public int getId() {
        return id;
    }

    public double getSalario() {
        return salario;
    }

    public String getCargo() {
        return cargo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
}

