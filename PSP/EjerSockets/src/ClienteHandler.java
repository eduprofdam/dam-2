import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClienteHandler extends Thread {
    private Socket socket;

    public ClienteHandler(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try (ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
             DataInputStream dis = new DataInputStream(socket.getInputStream())) {

            // Aseg�rate de hacer un flush despu�s de inicializar ObjectOutputStream
            oos.flush();

            // Lee el ID y el salario
            int id = dis.readInt();
            double salario = dis.readDouble();

            // Crea y env�a el objeto Empleado
            Empleado empleado = new Empleado(id, salario);
            oos.writeObject(empleado);

            System.out.println("Empleado procesado y enviado de vuelta al cliente.");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
