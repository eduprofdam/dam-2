import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class Cliente {
    

    public static void main(String[] args) {
        final String HOST = "localhost";
        final int PUERTO = 12345;
        ArrayList<Empleado> jefes = new ArrayList<>();
        ArrayList<Empleado> empleados = new ArrayList<>();

        try (Socket socket = new Socket(HOST, PUERTO);
             DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
             ObjectInputStream ois = new ObjectInputStream(socket.getInputStream())) {

            Random random = new Random();
            int id = random.nextInt(1000); // Genera un ID aleatorio
            double salario = 500 + random.nextDouble() * 2000; // Genera un salario aleatorio

            dos.writeInt(id);
            dos.writeDouble(salario);

            Empleado empleadoRecibido = (Empleado) ois.readObject();
            System.out.println("Empleado recibido: " + empleadoRecibido.getId());

            // Clasifica y almacena el empleado en la lista correspondiente
            if (empleadoRecibido.getSalario() > 1000) {
                empleadoRecibido.setCargo("Jefe");
                jefes.add(empleadoRecibido);
                leerLista(jefes, "Jefes");
            } else {
                empleadoRecibido.setCargo("Empleado");
                empleados.add(empleadoRecibido);
                leerLista(empleados, "Empleados");
            }

           
            

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void leerLista(ArrayList<Empleado> lista, String tipo) {
        System.out.println("Lista de " + tipo + ":");
        for (Empleado e : lista) {
            System.out.println("ID: " + e.getId() + ", Salario: " + e.getSalario() + ", Cargo: " + e.getCargo());
        }
    }
}
