import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
    public static void main(String[] args) {
        final int PUERTO = 12345;
        ServerSocket servidorSocket = null;
        Socket socket = null;
        DataInputStream dis = null;
        ObjectOutputStream oos = null;

        try {
            servidorSocket = new ServerSocket(PUERTO);
            System.out.println("Servidor iniciado...");

            while (true) {
                socket = servidorSocket.accept();
                System.out.println("Cliente conectado.");

                new ClienteHandler(socket).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (oos != null) oos.close();
                if (dis != null) dis.close();
                if (socket != null) socket.close();
                if (servidorSocket != null) servidorSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

