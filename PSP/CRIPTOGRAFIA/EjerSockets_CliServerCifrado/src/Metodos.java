import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class Metodos {
	
	// Generar una clave secreta
    public static SecretKey generarSecretKey() throws Exception {
        KeyGenerator GeneradorKey = KeyGenerator.getInstance("AES");
        GeneradorKey.init(128); // Tama�o de la clave en bits (puede ser 128, 192 o 256)
        return GeneradorKey.generateKey();
    }

    // Cifrar el texto
    public static String encriptar(String textOriginal, SecretKey secretKey) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] BytesEncriptados = cipher.doFinal(textOriginal.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(BytesEncriptados);
    }

    // Descifrar el texto
    public static String desencriptar(String textoEncriptado, SecretKey secretKey) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] BytesEncriptados = Base64.getDecoder().decode(textoEncriptado);
        byte[] BytesDesencriptados = cipher.doFinal(BytesEncriptados);
        return new String(BytesDesencriptados, StandardCharsets.UTF_8);
    }

}
