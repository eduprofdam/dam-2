
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

public class Servidor {

    private static SecretKey secretKey;

    public static void main(String[] args) throws Exception {
        int port = 65000;
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("Servidor iniciado en el puerto " + port);

        
        secretKey = Metodos.generarSecretKey();

        // Aceptar una conexi�n de cliente
        Socket socket = serverSocket.accept();
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

        try (ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream input = new ObjectInputStream(socket.getInputStream())) {

            // Enviar fecha cifrada
        	Date d = new Date();
        	String timeStamp = d.toString();
            //String timeStamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
            String encryptedTime = Metodos.encriptar(timeStamp, secretKey);
            dos.writeUTF(encryptedTime);

            // Recibir objeto Datos del cliente
            Datos datosRecibidos = (Datos) input.readObject();
            System.out.println("Recibido del cliente: " + datosRecibidos);
            System.out.println("Recibido del cliente: " + datosRecibidos);
            String fechaDesencriptada = Metodos.desencriptar(datosRecibidos.getFechaCifrada(), secretKey);
           System.out.println(fechaDesencriptada);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            socket.close();
            serverSocket.close();
        }
    }

    public static String encrypt(String strToEncrypt, SecretKey secretKey) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        } catch (Exception e) {
            System.out.println("Error while encrypting: " + e.toString());
            return null;
        }
    }
}
