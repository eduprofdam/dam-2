
import java.io.*;
import java.net.Socket;
import java.util.Scanner;

import javax.crypto.SecretKey;

public class Cliente {

	public static void main(String[] args) throws Exception {
		String host = "localhost";
		int port = 65000;

		try (Socket socket = new Socket(host, port);
				DataInputStream dis = new DataInputStream(socket.getInputStream());
				ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
				Scanner scanner = new Scanner(System.in)) {

			// Recibir fecha cifrada del servidor
			String fechaCifrada = dis.readUTF();
			System.out.println("Fecha cifrada recibida del servidor: " + fechaCifrada);
			
			// Leer nombre e ID desde el teclado
			System.out.print("Introduce tu nombre: ");
			String nombre = scanner.nextLine();
			System.out.print("Introduce tu ID: ");
			String id = scanner.nextLine();

			// Crear objeto Datos y enviarlo al servidor
			Datos datos = new Datos(nombre, id, fechaCifrada);
			output.writeObject(datos);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
