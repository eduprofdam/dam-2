
import java.io.Serializable;

public class Datos implements Serializable {
    private String nombre;
    private String id;
    private String fechaCifrada; // La fecha cifrada como String

    // Constructor
    public Datos(String nombre, String id, String fechaCifrada) {
        this.nombre = nombre;
        this.id = id;
        this.fechaCifrada = fechaCifrada;
    }

    // Getters y Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFechaCifrada() {
        return fechaCifrada;
    }

    public void setFechaCifrada(String fechaCifrada) {
        this.fechaCifrada = fechaCifrada;
    }

    @Override
    public String toString() {
        return "Datos{" +
                "nombre='" + nombre + '\'' +
                ", id='" + id + '\'' +
                ", fechaCifrada='" + fechaCifrada + '\'' +
                '}';
    }
}
