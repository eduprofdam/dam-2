import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class Metodos {
	
	
	
	// Para generar una clave secreta
	public static SecretKey generarSecretKey(int tama�o) throws NoSuchAlgorithmException {
		
		
		KeyGenerator GeneradorKey = KeyGenerator.getInstance("AES");
		
		GeneradorKey.init(tama�o);  // 128 - 192 - 256 
		
		return GeneradorKey.generateKey();
		
	}
	
	
	//Para encriptar mensajes
	public static String  encriptar(String textoOriginal, SecretKey secretKey) throws Exception {
		
		Cipher cipher = Cipher.getInstance("AES");
		
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		
		byte [] BytesEncriptados = cipher.doFinal(textoOriginal.getBytes(StandardCharsets.UTF_8));
		
		return Base64.getEncoder().encodeToString(BytesEncriptados);
		
	}
	
	
	public static String desencriptar (String textoEncriptado, SecretKey secretKey) throws Exception {
		
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte [] bytesEncriptados = Base64.getDecoder().decode(textoEncriptado);
		byte [] bytesDesencriptados = cipher.doFinal(bytesEncriptados);
		
		String mensaje_desencriptado = new String (bytesDesencriptados,StandardCharsets.UTF_8);
		
		return mensaje_desencriptado;
	}
	

}
