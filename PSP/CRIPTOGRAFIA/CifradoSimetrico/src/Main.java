import java.security.NoSuchAlgorithmException;

import javax.crypto.SecretKey;

public class Main {

	public static void main(String[] args) {
		
		//Genero la clave secreta
		
		try {
			SecretKey secretKey = Metodos.generarSecretKey(256);
			
			//Mensaje a cifrar
			String textoOriginal = "Hola alumnos de dam";
			
			String textoEncriptado = Metodos.encriptar(textoOriginal,secretKey);
			
			System.out.println("Texto cifrado es: "+ textoEncriptado);
			
			
			//Desciframos el mensaje
			
			String textoDesencriptado = Metodos.desencriptar(textoEncriptado, secretKey);
			
			System.out.println("Texto descifrado es : "+ textoDesencriptado);
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
