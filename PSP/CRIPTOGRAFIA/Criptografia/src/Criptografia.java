import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Criptografia {

	public static void main(String[] args) {
		
		String mensajeOriginal = "Hola alumnos de DAM";
		
		String algoritmo = "SHA-256";
		
		try {
			
			// Obtenemos la instancia con el algoritmo seleccionado
			
			MessageDigest  md =  MessageDigest.getInstance(algoritmo);
			
			// Convierto el mensaje original en un arreglo de bytes
			byte [] byteMensaje = mensajeOriginal.getBytes();
			
			
			// Calcular el hash del mensaje
			
			byte [] resumen = md.digest(byteMensaje);
			
			// Convertir  el resumen a una representacion hexadecimal
			
			StringBuilder hexString = new StringBuilder();
			
			for(byte b : resumen) {
				String hex = Integer.toHexString(0xFF & b);
				
				// Si el  byte tiene un digito le añade  un 0. Ejemplo: 0F,en lugar de F
				if  (hex.length()==1) {
					hexString.append('0');
				}
				hexString.append(hex);
			}
			
			// Imprimir el mensaje original y el hash
			
			System.out.println("Mensaje original: "+ mensajeOriginal);
			System.out.println("Hash: "+ algoritmo + ": "+ hexString.toString());
			
			
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Algoritmo no encontrado");
			e.printStackTrace();
		}

	}

}
