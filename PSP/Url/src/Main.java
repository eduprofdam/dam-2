import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Main {

	public static void main(String[] args) throws IOException {
		
		final int CARACTERES = 550;
		
		try {
			URL direccion = new URL("https://www.marca.com/futbol/real-madrid/2024/02/09/65bf7f1146163f821e8b45ab.html");
			
			System.out.println("El protocolo utilizado es : "+ direccion.getProtocol());
			System.out.println("El host es : "+ direccion.getHost());
			//System.out.println("El puerto es : "+ direccion.getPort());
			//System.out.println("El ficheroe es : "+ direccion.getFile());
			
			//Establecemos la conexion
			URLConnection conexion = direccion.openConnection();
			
			// Abrimos un flujo de entrada de datos
			BufferedReader reader = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
			/*
			char [] buffer = new char[CARACTERES];
			int numCharsRead = reader.read(buffer,0,CARACTERES);
			
			System.out.println("Primeros"+ numCharsRead + "caracteres del contenido");
			String texto = new String(buffer,0,numCharsRead);
			System.out.println(texto);
			
			*/
			
			String urlString = " ";
			String actual;
			
			while((actual = reader.readLine()) != null) {
				urlString +=actual;
			}
			System.out.println(urlString);
			
			// Cierro el buffer
			reader.close();
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
