

import java.io.IOException;
import java.sql.Date;
import java.util.concurrent.TimeUnit;

public class Ejer1 {

	public static void main(String[] args) throws IOException, InterruptedException {
		
		String pr = "notePad.exe";
		ProcessBuilder pb = new ProcessBuilder(pr);
		Process process = pb.start();
		while (process.isAlive()) {
			Date date = new Date(0);
			System.out.println(date+ "El proceso "+process.pid() + "sigue vivo");
			process.waitFor(5, TimeUnit.SECONDS);
			
		}
		System.out.println("El proceso ha finalizado");
	}

}
