import java.io.File;
import java.io.IOException;

public class EjemploRedirect {

	public static void main(String[] args) {
		
		ProcessBuilder cmd = new ProcessBuilder("cmd","/c","dir");
		
		File f = new File ("salida.txt");
		try {
			f.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cmd.redirectOutput(f); // guardamos la info del proceso en el fichero f
		
		cmd.redirectInput(f); // Dejamos la info del f en memori
		File f2 = new File ("salida2.txt");
		cmd.redirectOutput(f2); // guardamos la info de memoria en f2
		
		
		
		try {
			Process  p= cmd.start();
		} catch (IOException e) {
			System.out.println("No ha podido iniciarse el proceso");
			e.printStackTrace();
		}
	}

}
