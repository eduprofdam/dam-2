package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Cliente {

	public static void main(String[] args) throws IOException {
		
		final int  PUERTO_SERVIDOR = 5000;
		// Buffer para almacenar los mensajes
		byte[] buffer = new byte[1024];
		
		try {
			// Obtengo la localizacion de localhost
			InetAddress direccionServidor = InetAddress.getByName("localhost");
			
			// Creo el socket UDP
			DatagramSocket socketUDP = new DatagramSocket();
			String mensaje = "Hola, buenos dias desde el cliente";
			
			// Convierto el mensaje a bytes
			
			buffer = mensaje.getBytes();
			
			// cREO el datagram
			DatagramPacket pregunta = new DatagramPacket(buffer, buffer.length,direccionServidor,PUERTO_SERVIDOR);
			
			// Envío datos
			System.out.println("Envío el datagrama");
			socketUDP.send(pregunta);
			
			// Preparo  la respuesta
			DatagramPacket peticion = new DatagramPacket(buffer,buffer.length);
			socketUDP.receive(peticion);
			System.out.println("Recibo la petición");
			
			// Cogemos los datos y los mostramos
			mensaje = new String(peticion.getData());
			System.out.println(mensaje);
			
			socketUDP.close();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
